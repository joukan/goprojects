package ipcon

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	"github.com/mdlayher/netlink"
)

func autoPort() uint32 {
	rand.Seed(int64(time.Now().Nanosecond()))
	return rand.Uint32()
}

type ipconChannel struct {
	ncCon    *netlink.Conn
	ncConfig *netlink.Config
	ptrIPH   *ipconHandler
	ui32Port uint32
	ncFlag   netlink.HeaderFlags
	mu       sync.Mutex
}

func (c ipconChannel) initialize(flag netlink.HeaderFlags) error {
	d, err := netlink.Dial(KIpconNetlinkProtocol, c.ncConfig)

	if err == nil {
		c.ncCon = d
		c.ui32Port = autoPort()
		c.ncFlag = flag
	}
	return err
}

func (c ipconChannel) portid() uint32 {
	return c.ui32Port
}

func (c ipconChannel) sendrcv(msg netlink.Message) ([]netlink.Message, error) {
	if c.ncFlag == 0 {
		return nil, fmt.Errorf("using a recever to send")
	}
	msg.Header.PID = c.ui32Port
	msg.Header.Flags = c.ncFlag
	return c.ncCon.Execute(msg)
}

type ipconHandler struct {
	sName         string
	icCtrlChan    ipconChannel
	icSendChan    ipconChannel
	icReceiveChan ipconChannel
	u32Flag       uint32
}

func autoPeerName() string {
	rand.Seed(int64(time.Now().Nanosecond()))
	return fmt.Sprintf("Anon-%d", rand.Intn(10000000))
}

func (h ipconHandler) initialize(peer string, flag uint32) error {

	if peer != "" {
		if len(peer) > KIpconMaxNameLen {
			return fmt.Errorf("Peer name is too lang")
		}
		h.sName = peer
	} else {
		h.u32Flag |= KIpconFlagAnonPeer
		h.sName = autoPeerName()
	}

	if (flag & IpconFlagDisableKeventFilter) != 0 {
		h.u32Flag |= KIpconFlagDisableKeventFilter
	}

	err := h.icCtrlChan.initialize(netlink.Request | netlink.Acknowledge)
	if err != nil {
		return err
	}

	err = h.icSendChan.initialize(netlink.Request | netlink.Acknowledge)
	if err != nil {
		return err
	}

	err = h.icReceiveChan.initialize(0)
	if err != nil {
		return err
	}

	ae := netlink.NewAttributeEncoder()
	ae.String(KIpconAttrPeerName, h.sName)
	ae.Uint32(KIpconAttrSendPort, h.icSendChan.portid())
	ae.Uint32(KIpconAttrReceivePort, h.icReceiveChan.portid())
	if h.u32Flag != 0 {
		ae.Uint32(KIpconAttrFlag, h.u32Flag)
	}

	d, err := ae.Encode()
	if err != nil {
		return err
	}

	msg := netlink.Message{
		netlink.Header{
			Length:   0,
			Type:     KIpconPeerReg,
			Flags:    0,
			Sequence: 0,
			PID:      0,
		},
		d,
	}
	_, err = h.icCtrlChan.sendrcv(msg)

	return err
}

func (h ipconHandler) isPeerPresent(peer string) bool {
	h.icCtrlChan.mu.Lock()
	defer h.icCtrlChan.mu.Unlock()

	if !kvalidName(peer) {
		return false
	}

	ae := netlink.NewAttributeEncoder()
	ae.String(KIpconAttrPeerName, peer)
	d, err := ae.Encode()
	if err != nil {
		return false
	}

	msg := netlink.Message{
		netlink.Header{
			Length:   0,
			Type:     KIpconPeerReslove,
			Flags:    0,
			Sequence: 0,
			PID:      0,
		},
		d,
	}

	_, err = h.icCtrlChan.sendrcv(msg)

	return err == nil
}

func (h ipconHandler) isGroupPresent(peer string, group string) bool {
	return false
}

func (h ipconHandler) receiveMsg() (*IpconMsg, error) {
	var msg IpconMsg
	return &msg, nil
}

func (h ipconHandler) sendUnicast(peer string, d []byte) error {
	return nil
}

func (h ipconHandler) sendMulticast(peer string,
	group string, d []byte, sync bool) error {
	return nil
}

func (h ipconHandler) registerGroup(group string) error {
	h.icCtrlChan.mu.Lock()
	defer h.icCtrlChan.mu.Unlock()

	if !kvalidName(group) {
		return fmt.Errorf("Invalid group name : %s", group)
	}

	ae := netlink.NewAttributeEncoder()
	ae.String(KIpconAttrGroupName, group)
	d, err := ae.Encode()
	if err != nil {
		return err
	}

	msg := netlink.Message{
		netlink.Header{
			Length:   0,
			Type:     KIpconGroupRegister,
			Flags:    0,
			Sequence: 0,
			PID:      0,
		},
		d,
	}

	_, err = h.icCtrlChan.sendrcv(msg)

	return err
}

func (h ipconHandler) joinGroup(peer string, group string) error {
	return nil
}
