package ipcon

const (
	KIpconNetlinkProtocol  = 29
	KIpconKernelName       = "ipcon"
	KIpconKernelGroupName  = "ipcon_kevent"
	KIpconMaxPayloadLength = 2048
	KIpconMaxNameLen       = 32
	KIpconMaxKernelGroup   = 128
)
const (
	KPeerTypeAnon = iota
	KPeerTypeNormal
	KPeerTypeMax
)

const (
	KIpconPeerReg = iota + 100
	KIpconPeerReslove
	KIpconGroupRegister
	KIpconGroupUnregister
	KIpconGroupReslove
	KIpconCtlCmdMax
	KIpconUsrMsg
	KIpconMulticastMsg
	KIpconTypeMax
)

const (
	KIpconFlagAnonPeer = 1 << iota
	KIpconFlagMulticastSync
	KIpconFlagDisableKeventFilter
)

const (
	KIpconAttrUNSPEC = iota
	KIpconAttrCtrlPort
	KIpconAttrSendPort
	KIpconAttrReceivePort
	KIpconAttrGroup
	KIpconAttrPeerName
	KIpconAttrGroupName
	KIpconAttrData
	KIpconAttrFlag
	KIpconAttrAfterLast
)

type KIpconMsgHdr struct {
	reserved uint32
}

func kvalidIpconGroup(group uint32) bool {
	if group < KIpconMaxKernelGroup-1 {
		return true
	}
	return false
}

func kvalidUserIpconGroup(group uint32) bool {
	return (group > 0 && kvalidIpconGroup(group))
}

func kvalidName(name string) bool {
	if len(name) > KIpconMaxNameLen || len(name) == 0 {
		return false
	}

	return true
}

const (
	KIpconEventPeerAdd = iota
	KIpconEventPeerRemove
	KIpconEventGroupAdd
	KIpconEventGroupRemove
)

type KIpconKevent struct {
	EventType int
	PeerName  string
	GroupName string
}
