package ipcon

import (
	"fmt"
	"log"

	"golang.org/x/sys/unix"
)

var (
	// IPCONLoggerErr : Error log handler
	IPCONLoggerErr *log.Logger
	// IPCONLoggerWarn : Warning log handler
	IPCONLoggerWarn *log.Logger

	// IPCONLoggerInfo : Information log handler
	IPCONLoggerInfo *log.Logger

	// IPCONLoggerDebug : Debug log handler
	IPCONLoggerDebug *log.Logger
)

func init() {
	IPCONLoggerErr = NewErrLogger()
	IPCONLoggerWarn = NewWarnLogger()
	IPCONLoggerInfo = NewInfoLogger()
	IPCONLoggerDebug = NewDebugLogger()
}

const (
	IpconNormalMsg = iota
	IpconGroupMsg
	IpconKeventMsg
	IpconInvalidMsg
)

const (
	IpconMaxUserGroup = 5
)

const (
	IRFlagAckOK = 1 << iota
)

type IpconMsgResult struct {
	msg *unix.NLMsghdr
}

type IpconMsg struct {
	CmdType   uint32
	PeerGroup string
	Buffer    []byte
	Length    uint32
}


type IpconGroupInfo struct {
	GrpName string
	SrvName string
	GrpID   uint32
}

const (
	IpconFlagAnonPeer = 1 << iota
	IpconFlagDisableKeventFilter
	IpconFlagAsyncIO
)

type IpconH interface {
	isPeerPresent(peer string) bool
	isGroupPresent(peer string, group string) bool
}

//IsPeerPresent : return true if peer exist
func IsPeerPresent(h IpconH, peer string) (bool, error) {
	switch v := h.(type) {
	case ipconHandler:
		return v.isPeerPresent(peer), nil
	default:
		return false, fmt.Errorf("Invalid handler")
	}
}

func IsGroupPresent(h IpconH, peer string, group string) (bool, error) {
	switch v := h.(type) {
	case ipconHandler:
		return v.isGroupPresent(peer, group), nil
	default:
		return false, fmt.Errorf("Invalid handler")
	}

}

func ReceiveMsg(h IpconH) (*IpconMsg, error) {
	switch v := h.(type) {
	case ipconHandler:
		return v.receiveMsg()
	default:
		return nil, fmt.Errorf("Invalid handler")
	}
}

func SendUnicast(h IpconH, peer string, d []byte) error {
	switch v := h.(type) {
	case ipconHandler:
		return v.sendUnicast(peer, d)
	default:
		return fmt.Errorf("Invalid handler")
	}

}

func SendMulticast(h IpconH, peer string, group string, d []byte, sync bool) error {
	switch v := h.(type) {
	case ipconHandler:
		return v.sendMulticast(peer, group, d, sync)
	default:
		return fmt.Errorf("Invalid handler")
	}

}

func RegisterGroup(h IpconH, group string) error {
	switch v := h.(type) {
	case ipconHandler:
		return v.registerGroup(group)
	default:
		return fmt.Errorf("Invalid handler")
	}
}

func JoinGroup(h IpconH, peer string, group string) error {
	switch v := h.(type) {
	case ipconHandler:
		return v.joinGroup(peer, group)
	default:
		return fmt.Errorf("Invalid handler")
	}
}

func CreateIpconHandler(peer string, flag uint32) (IpconH, error) {
	var h ipconHandler
	err := h.initialize(peer, flag)
	if err != nil {
		return nil, err
	}

	return h, nil
}
